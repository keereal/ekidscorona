# Clone and start index.html )

or visit [http://ekids.kyryl.info](http://ekids.kyryl.info)

### have fun :)

The game has been created as part of the eKids volunteer initiative aimed at introducing children to internet technologies.

eKids is a volunteer initiative that aims to teach children about internet technologies in a fun and engaging way. The game was created as a part of this initiative, with the goal of providing an interactive and educational experience for children. Through the game, children can learn about various internet technologies, such as coding, web development, and adaptive design. By making learning about technology enjoyable, eKids hopes to inspire children to pursue careers in technology and become active participants in shaping the future of the internet.