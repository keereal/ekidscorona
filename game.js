const game = {
  class: ".game",
  time: 85,
  virusAmount: 145,
  zone: {
    width: 96 /*percent*/,
    height: 92,
  },
  points: 0,
  addPoint() {
    this.points++;
  },
  viruses: ["red", "green", "blue", "magenta"],
  randomVirus() {
    return this.viruses[Math.floor(Math.random() * this.viruses.length)];
  },
};

const virus = {
  createVirusUI() {
    const gameDiv = document.querySelector(`${game.class}-zone`);
    const div = document.createElement("div");
    div.className = `game-virus game-virus-born game-virus--${game.randomVirus()}`;
    div.setAttribute(
      "style",
      `
        left: ${Math.floor(Math.random() * game.zone.width)}%;
        bottom: ${Math.floor(Math.random() * game.zone.height)}%;
        z-index: ${6000 + Math.floor(Math.random() * 100)};
        `
    );
    setTimeout(() => {
      div.classList.remove("game-virus-born");
      div.classList.add("game-virus-live");
    }, 1250);
    gameDiv.appendChild(div);
  },
  deleteVirusUI(e) {
    if (e.target.classList.contains("game-virus")) {
      e.target.classList.add("game-virus-kill");
      const soundKill = new Audio("./audio/kill.mp3");
      soundKill.play();
      e.target.style.pointerEvents = "none";
      setTimeout(() => {
        e.target.remove();
      }, 900);
      game.addPoint();
    }
  },
};

function showAlert(message) {
  alert(message);
  location.reload();
}

function gameOver() {
  const timer = document.querySelector(".game-timer-counter");
  timer.innerHTML = "00:00";
  document.querySelector(".game-zone").style.visibility = "hidden";
  setTimeout(() => {
    showAlert("YOU LOSE!!!!!");
  }, 100);
}

function startGameTimer(time = game.time) {
  const timer = document.querySelector(".game-timer-counter");
  const counter = setInterval(() => {
    time--;
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    timer.innerHTML = `${minutes < 10 ? "0" : ""}${minutes}:${
      seconds < 10 ? "0" : ""
    }${seconds}`;
    if (time < 1) {
      clearInterval(counter);
      timer.innerHTML = "00:00";
      gameOver();
    }
  }, 1000);
}

function createViruses(amount = game.virusAmount) {
  for (let i = 0; i < amount; i++) {
    setTimeout(() => {
      virus.createVirusUI();
    }, 30 * i);
  }
}

//Events
document.querySelector(".game-button").addEventListener("click", (e) => {
  document.querySelector(".game-greetings").remove();
  const music = new Audio("./audio/music.mp3");
  music.play();
  createViruses();
  startGameTimer();
  e.currentTarget.remove();
});

document.querySelector(".game-zone").addEventListener("click", (e) => {
  virus.deleteVirusUI(e);
  const pointsDisplay = document.querySelector(".game-points-counter");
  pointsDisplay.textContent = game.points;
  if (game.points === game.virusAmount) {
    setTimeout(() => {
      showAlert("YOU WIN!!!!!");
    }, 1500);
  }
});
